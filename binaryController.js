'use strict';

exports.binarySort = (req, res) => {
    if (req.body.hasOwnProperty('nums')){
        const nums = req.body.nums;
        const binaries = {};
        nums.forEach((num) => {
            naturalToBinary(num, binaryRep => {
                binaries[num] = binaryRep;
            });
        });

        let sortableBinaries = [];
        Object.values(binaries).forEach(element => {
            sortableBinaries.push(element);
        });

        let sortable = [];
        for (let binary in binaries) {
            sortable.push(binary)
        }
        console.log('sortableNaturals', sortable);
        console.log('sortableBinaries', sortableBinaries);
        let sorted = sortableBinaries.sort(sortByBinaryReference);
        console.log('Sorted', sorted);

        sorted = sorted.map(sortedElement => {
            return Object.keys(binaries).find(element => {
                return binaries[element] === sortedElement;
            });
        });

        sorted = sorted.map(element => {return Number(element)});

        return res.status(200).json({sorted: sorted});
    }

    res.status(400).json({code: 400, message: 'Enter an array numbers in "nums" key. Ex: {"nums":[1, 2, 3, 4, 5]}'});
};

/**
 * Custom binary representation sort
 * @param a
 * @param b
 * @returns {number}
 */
let sortByBinaryReference = (a, b) => {
    let onsInA = 0;
    let onsInB = 0;

    for (let i = 0; i <= a.length; i++) {
        if (a[i] === '1') {
            onsInA++;
        }
    }
    for (let i = 0; i <= b.length; i++) {
        if (b[i] === '1') {
            onsInB++;
        }
    }

    return(onsInB - onsInA)
};

/**
 * Convert num in binary string representation
 * @param num
 * @param callback
 */
let naturalToBinary = (num, callback) => {

    if (num != Math.floor(num)) {
        console.log("Please enter a number");
        callback({status: 400, message: 'numero invalido'});

    } else if (num < 0) {
        console.log("Please enter a positive number");
        callback({status: 400, message: 'numero invalido'});

    } else {
        let binary = parseInt(num, 10);
        callback(binary.toString(2));

    }
};