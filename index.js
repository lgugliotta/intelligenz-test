'use strict';

var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    port = process.env.PORT || '3000';

app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados

var router = express.Router();
var binary = require('./binaryController');

router.post('/', binary.binarySort);
app.use(router);

app.listen(port);
console.log('Listening on port: ' + port);

